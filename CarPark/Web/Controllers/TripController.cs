﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using System.Net.Http;
using System;
using Newtonsoft.Json;
using System.Text;
using System.Collections.Generic;
using System.Net.Http.Headers;
using Model.Entities;
using Model.ViewModels;

namespace Web.Controllers
{
    public class TripController : BaseController
    {
        public const string BASE_URL_API = "https://localhost:5001";
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IConfiguration _configuration;

        public TripController(IHttpClientFactory httpClientFactory,
                         IHttpContextAccessor httpContextAccessor,
                          IConfiguration configuration)
        {
            _httpContextAccessor = httpContextAccessor;
            _configuration = configuration;
            _httpClientFactory = httpClientFactory;
        }
        public IActionResult Index()
        {
            ViewBag.Thispage = 1;
            return View();
        }

        public ActionResult GetData(string key = "", int year = 0, int month = 0, int day = 0, string type = "STT", int pagesize = 3, int page = 1)
        {
            try
            {
                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration["BaseAddress"]);
                var sessions = _httpContextAccessor
         .HttpContext
         .Session
         .GetString("Token");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);
                HttpResponseMessage response = Task.Run(async () => await client.GetAsync("/api/Trip/" + type + "/" + pagesize + "/" + page + "?key="+ key + "&year=" + year + "&month=" + month + "&day=" + day)).Result;
                var test = response.Content.ReadAsStringAsync().Result;

                var listdata = new List<Trip>();
                ViewBag.Thispage = page;

                if (response.IsSuccessStatusCode)
                {
                    listdata = JsonConvert.DeserializeObject<List<Trip>>(test.ToString());

                    return PartialView("_PartialData", listdata);
                }

                return PartialView("_PartialData", listdata);

            }
            catch (Exception ex)
            {
                return PartialView("_PartialData", null);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> CreateAsync(Trip vm)
        {
            try
            {
                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration["BaseAddress"]);
                var sessions = _httpContextAccessor
          .HttpContext
          .Session
          .GetString("Token");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);

                if (ModelState.IsValid)
                {
                    var createBy = User != null ? User.Identity.Name : "Admin";
                    var requestContent = new MultipartFormDataContent();
                    requestContent.Add(new StringContent(vm.TicketNumber.ToString()), "TicketNumber");
                    requestContent.Add(new StringContent(vm.CarType.ToString()), "CarType");
                    requestContent.Add(new StringContent(vm.DepartureTime.ToString()), "DepartureDate");
                    requestContent.Add(new StringContent(vm.DepartureTime.ToString()), "DepartureTime");
                    requestContent.Add(new StringContent(vm.Destination.ToString()), "Destination");
                    requestContent.Add(new StringContent(vm.Driver.ToString()), "Driver");
                    requestContent.Add(new StringContent(vm.MaxTicketNumerOnline.ToString()), "MaxTicketNumerOnline");
                    string url = string.Empty;
                    url = _configuration["BaseAddress"] + "/api/Trip";
                    var response = await client.PostAsync(url, requestContent);
                    if (response.IsSuccessStatusCode)
                    {
                        TempData["message"] = "Thêm mới thành công";
                        return RedirectToAction(nameof(Index));
                    }
                    else
                    {
                        TempData["error"] = "Lỗi thêm mới";
                        return RedirectToAction(nameof(Index), new { vm });

                    }
                }
                TempData["error"] = "Lỗi thêm mới";
                return RedirectToAction(nameof(Index), new { vm });
            }
            catch
            {
                TempData["error"] = "Lỗi thêm mới";
                return RedirectToAction(nameof(Index), new { vm });
            }
        }

        public async Task<JsonResult> Detail(int id)
        {
            try
            {
                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration["BaseAddress"]);
                var sessions = _httpContextAccessor
          .HttpContext
          .Session
          .GetString("Token");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);
                client.BaseAddress = new Uri(_configuration["BaseAddress"]);
                string url = string.Empty;
                url = _configuration["BaseAddress"] + "/api/Trip/" + id;
                HttpResponseMessage response = await client.DeleteAsync(url);
                Trip data = new Trip();
                if (response.IsSuccessStatusCode)
                {
                    var txt_res = response.Content.ReadAsStringAsync().Result;
                    data = JsonConvert.DeserializeObject<Trip>(txt_res.ToString());
                    return Json(data);
                }
                return Json("Error");
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi";
                return Json("Lỗi");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditAsync(Trip vm)
        {
            try
            {
                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration["BaseAddress"]);
                var sessions = _httpContextAccessor
          .HttpContext
          .Session
          .GetString("Token");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);

                if (ModelState.IsValid)
                {
                    var createBy = User != null ? User.Identity.Name : "Admin";
                    var requestContent = new MultipartFormDataContent();
                    requestContent.Add(new StringContent(vm.Id.ToString()), "Id");
                    requestContent.Add(new StringContent(vm.TicketNumber.ToString()), "TicketNumber");
                    requestContent.Add(new StringContent(vm.CarType.ToString()), "CarType");
                    requestContent.Add(new StringContent(vm.DepartureTime.ToString()), "DepartureDate");
                    requestContent.Add(new StringContent(vm.DepartureTime.ToString()), "DepartureTime");
                    requestContent.Add(new StringContent(vm.Destination.ToString()), "Destination");
                    requestContent.Add(new StringContent(vm.Driver.ToString()), "Driver");
                    requestContent.Add(new StringContent(vm.MaxTicketNumerOnline.ToString()), "MaxTicketNumerOnline");
                    string url = string.Empty;
                    url = _configuration["BaseAddress"] + "/api/Trip/" + vm.Id;
                    var response = await client.PutAsync(url, requestContent);
                    if (response.IsSuccessStatusCode)
                    {
                        TempData["message"] = "Cập nhật thành công";
                        return RedirectToAction(nameof(Index));
                    }
                    else
                    {
                        TempData["error"] = "Lỗi Cập nhật";
                        return RedirectToAction(nameof(Index), new { vm });
                    }
                }
                TempData["error"] = "Lỗi Cập nhật";
                return RedirectToAction(nameof(Index), new { vm });
            }
            catch
            {
                TempData["error"] = "Lỗi Cập nhật";
                return RedirectToAction(nameof(Index), new { vm });
            }
        }
        public async Task<JsonResult> Xoa(long id)
        {
            try
            {
                var sessions = _httpContextAccessor
                               .HttpContext
                               .Session
                               .GetString("Token");
                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri(_configuration["BaseAddress"]);
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);
                string url = string.Empty;
                url = _configuration["BaseAddress"] + "/api/Trip/" + id;
                HttpResponseMessage response = await client.DeleteAsync(url);
                if (response.IsSuccessStatusCode)
                {
                    return Json("OK");
                }
                return Json("Error");
            }
            catch (Exception ex)
            {
                TempData["error"] = "Lỗi xóa: " + ex.Message;
                return Json("Lỗi xóa");
            }
        }

    }
}