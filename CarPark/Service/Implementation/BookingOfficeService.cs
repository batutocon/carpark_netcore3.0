﻿using Service.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using Model.Entities;
using System.Threading.Tasks;
using Model;
using System.Linq;
using Service.Repository;
using Model.ViewModels;
using Microsoft.EntityFrameworkCore;
using Model.Stored;

namespace Service.Implementation
{
    public class BookingOfficeService : IBookingOffice
    {
        protected readonly IGenericRepository<BookingOffice> _context;

        public BookingOfficeService(IGenericRepository<BookingOffice> context)
        {
            _context = context;
        }

        public IEnumerable<BookingOffice> Filter(string key = "", string type = "STT", int pagesize = 3, int page = 1)
        {
            var lstObj = _context.Querry().Include(x => x.Trip).Where(x=>x.Name.Contains(key)).Skip((page - 1) * pagesize).Take(pagesize).ToList();            
            if (type == "Name")
            {
                return lstObj.OrderBy(x => x.Name);
            }
            else if (type == "STT")
            {
                return lstObj.OrderBy(x => x.Id);
            }
            else
            {
                return lstObj;
            }
        }

        IEnumerable<BookingOffice> IBookingOffice.GetAll()
        {
            var lstObj = _context.Querry().Include(x => x.Trip).ToList();
            return lstObj;
        }

        public BookingOffice GetById(int id)
        {
            var obj = _context.Querry().Include(x => x.Trip).FirstOrDefault(x=>x.Id==id);
            return obj;
        }

        public async Task<int> Add(BookingOffice entity)
        {
            entity.Id = 0;
            //var rs = _context.Add(entity).Result;
            return (0);      
        }

        public async Task<int> Update(BookingOffice entity)
        {
            // var rs = _context.Update(entity).Result;
            // return (rs);
            return (0);
        }

        public void Remove(BookingOffice entity)
        {
            _context.Remove(entity);
        }

        //public IEnumerable<BookingOfficesDaCoTrip> GetResultBySqlProcedure(string query, params object[] parameters)
        //{
        //    if (parameters != null)
        //    {
        //        IEnumerable<BookingOfficesDaCoTrip> rs = (IEnumerable<BookingOfficesDaCoTrip>)_carpark.Set<BookingOfficesDaCoTrip>().FromSqlRaw(query, parameters).ToList();
        //        return rs;
        //    }
        //    else
        //    {
        //        IEnumerable<BookingOfficesDaCoTrip> rs = (IEnumerable<BookingOfficesDaCoTrip>)_carpark.Set<BookingOfficesDaCoTrip>().FromSqlRaw(query, parameters).ToList();
        //        return rs;
        //    }
        //}
    }
}
