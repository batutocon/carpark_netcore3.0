﻿using Service.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using Model.Entities;
using System.Threading.Tasks;
using Model;
using System.Linq;
using Service.Repository;

namespace Service.Implementation
{
    public class TripService : ITrip
    {
        protected readonly IGenericRepository<Trip> _context;

        public TripService(IGenericRepository<Trip> context)
        {
            _context = context;
        }
        public async Task<int> Add(Trip entity)
        {
            entity.Id = 0;
            //var rs = _context.Add(entity).Result;
            //return (rs);
            return (0);
        }
        public async Task<int> Update(Trip entity)
        {
            //var rs = _context.Update(entity).Result;
            //return (rs);
            return (0);
        }

        public IEnumerable<Trip> GetAll()
        {
            var lst = _context.GetAll();
            return lst;
        }

        Trip ITrip.GetById(int id)
        {
            var obj = _context.GetById(id);
            return obj;
        }

        void ITrip.Remove(Trip entity)
        {
            _context.Remove(entity);
        }
        public IEnumerable<Trip> Filter(string key, int year, int month, int day, string type, int pagesize, int page)
        {
            IQueryable<Trip> lstObj = Enumerable.Empty<Trip>().AsQueryable();
            if (year == 0)
            {
                lstObj = _context.Querry().Where(x => x.Destination.Contains(key)).Skip((page - 1) * pagesize).Take(pagesize);
            }
            else
            {
                if (month==0)
                {
                    lstObj = _context.Querry().Where(x => x.Destination.Contains(key) && x.DepartureDate.Date.Year == year).Skip((page - 1) * pagesize).Take(pagesize);
                }
                else
                {
                    if (day==0)
                    {
                        lstObj = _context.Querry().Where(x => x.Destination.Contains(key) && x.DepartureDate.Date.Year == year && x.DepartureDate.Date.Month == month).Skip((page - 1) * pagesize).Take(pagesize);
                    }
                    else
                    {
                        lstObj = _context.Querry().Where(x => x.Destination.Contains(key) && x.DepartureDate.Date.Year == year && x.DepartureDate.Date.Month == month && x.DepartureDate.Date.Day == day).Skip((page - 1) * pagesize).Take(pagesize);
                    }
                }
            }

            lstObj.ToList();

            if (type == "Destination")
            {
                return lstObj.OrderBy(x => x.Destination);
            }
            else if (type == "DepartureTime")
            {
                return lstObj.OrderBy(x => x.DepartureTime);
            }
            else if (type == "STT")
            {
                return lstObj.OrderBy(x => x.Id);
            }
            else
            {
                return lstObj;
            }
        }
    }
}
