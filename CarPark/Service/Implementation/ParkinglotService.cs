﻿using Service.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using Model.Entities;
using System.Threading.Tasks;
using Model;
using System.Linq;
using Service.Repository;
using Model.ViewModels;

namespace Service.Implementation
{
    public class ParkinglotService : IParkinglot
    {
        protected readonly IGenericRepository<Parkinglot> _context;

        public ParkinglotService(IGenericRepository<Parkinglot> context)
        {
            _context = context;
        }

        IEnumerable<Parkinglot> IParkinglot.Filter(string key, string type, int pagesize, int page)
        {
            var lstObj = _context.Querry().Where(x => x.Name.Contains(key)).Skip((page - 1) * pagesize).Take(pagesize);           
            if (type == "Name")
            {
                return lstObj.OrderBy(x => x.Name);
            }
            if (type == "STT")
            {
                return lstObj.OrderBy(x => x.Id);
            }
            else
            {
                return lstObj;
            }
        }

        public IEnumerable<Parkinglot> GetAll()
        {
            var lst = _context.GetAll();
            return lst;
        }

        public Parkinglot GetById(int id)
        {
            var obj = _context.GetById(id);
            return obj;
        }

        public async Task<int> Add(Parkinglot entity)
        {
            entity.Id = 0;
            //var rs = _context.Add(entity).Result;
            //return (rs);
            return (0);
        }

        public async Task<int> Update(Parkinglot entity)
        {
            //var rs = _context.Update(entity).Result;
            //return (rs);
            return (0);
        }

        public void Remove(Parkinglot entity)
        {
            _context.Remove(entity);
        }
    }
}
