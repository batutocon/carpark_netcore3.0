﻿using Service.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using Model.Entities;
using System.Threading.Tasks;
using Model;
using System.Linq;
using Service.Repository;
using Model.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace Service.Implementation
{
    public class TicketService : ITicket
    {
        protected readonly IGenericRepository<Ticket> _context;

        public TicketService(IGenericRepository<Ticket> context)
        {
            _context = context;
        }
        public async Task<int> Add(Ticket entity)
        {
            entity.Id = 0;
            //var rs = _context.Add(entity).Result;
            //return (rs);
            return (0);
        }
        public async Task<int> Update(Ticket entity)
        {
            //var rs = _context.Update(entity).Result;
            //return (rs);
            return (0);
        }

        IEnumerable<Ticket> ITicket.GetAll()
        {
            var lstObj = _context.Querry().Include(x => x.Trip).Include(x => x.Car).ToList();
            return lstObj;
        }

        Ticket ITicket.GetById(int id)
        {
            var obj = _context.GetById(id);
            return obj;
        }

        void ITicket.Remove(Ticket entity)
        {
            _context.Remove(entity);
        }

        public Ticket GetTicket(string otoid, int tripid)
        {
            var obj = new Ticket();
            obj = _context.Querry().Include(x => x.Trip).Include(x => x.Car).Where(x => x.LicensePlate == otoid && x.TripId == tripid).FirstOrDefault();
            return obj;
        }

        IEnumerable<Ticket> ITicket.Filter(string key, int year, int month, int day, string type, int pagesize, int page)
        {
            //var lstObj = _context.Querry().Include(x => x.Trip).Include(x => x.Car).Where(x => x.Trip.Destination.Contains(key) && x.BookAt.Date == searchDate.Date).Skip((page - 1) * pagesize).Take(pagesize).ToList();

            IQueryable<Ticket> lstObj = Enumerable.Empty<Ticket>().AsQueryable();
            if (year == 0)
            {
                lstObj = _context.Querry().Include(x => x.Trip).Include(x => x.Car).Where(x => x.Trip.Destination.Contains(key)).Skip((page - 1) * pagesize).Take(pagesize);
            }
            else
            {
                if (month == 0)
                {
                    lstObj = _context.Querry().Include(x => x.Trip).Include(x => x.Car).Where(x => x.Trip.Destination.Contains(key) && x.BookAt.Date.Year == year).Skip((page - 1) * pagesize).Take(pagesize);
                }
                else
                {
                    if (day == 0)
                    {
                        lstObj = _context.Querry().Include(x => x.Trip).Include(x => x.Car).Where(x => x.Trip.Destination.Contains(key) && x.BookAt.Date.Year == year && x.BookAt.Date.Month == month).Skip((page - 1) * pagesize).Take(pagesize);
                    }
                    else
                    {
                        lstObj = _context.Querry().Include(x => x.Trip).Include(x => x.Car).Where(x => x.Trip.Destination.Contains(key) && x.BookAt.Date.Year == year && x.BookAt.Date.Month == month && x.BookAt.Date.Day == day).Skip((page - 1) * pagesize).Take(pagesize);
                    }
                }
            }
            if (type == "Destination")
            {
                return lstObj.OrderBy(x => x.Trip.Destination);
            }
            else if (type == "STT")
            {
                return lstObj.OrderBy(x => x.Id);
            }
            else
            {
                return lstObj;
            }
        }
    }
}
