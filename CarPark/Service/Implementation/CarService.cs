﻿using Service.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using Model.Entities;
using System.Threading.Tasks;
using Model;
using System.Linq;
using Service.Repository;
using Model.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace Service.Implementation
{
    public class CarService : ICar
    {
        protected readonly IGenericRepository<Car> _context;

        public CarService(IGenericRepository<Car> context)
        {
            _context = context;
        }

        Car ICar.GetByLicensePlate(string id)
        {
            var obj = _context.Querry().Where(x => x.LicensePlate.Equals(id) == true).Include(x=>x.Parkinglot).FirstOrDefault();
            return obj;
        }

        IEnumerable<Car> ICar.Filter(string key, string type, int pagesize, int page)
        {
            var lstObj = _context.Querry().Include(x => x.Parkinglot).Where(x => x.LicensePlate.Contains(key)).Skip((page - 1) * pagesize).Take(pagesize).ToList();
            if (type == "LicensePlate")
            {
                return lstObj.OrderBy(x => x.LicensePlate);
            }
            else if (type == "Color")
            {
                return lstObj.OrderBy(x => x.Color);
            }
            else if (type == "Type")
            {
                return lstObj.OrderBy(x => x.Type);
            }
            else if (type == "STT")
            {
                return lstObj;
            }
            else
            {
                return lstObj;
            }
            //var lstObj = _context.GetListByParameter(x => x.LicensePlate.Contains(key)).Skip((page - 1) * pagesize).Take(pagesize).ToList();                
            //return lstObj;
        }

        public IEnumerable<Car> GetAll()
        {
            var lst = _context.GetAll();
            return lst;
        }

        BookingOffice ICar.GetById(int id)
        {
            throw new NotImplementedException();
        }

        //public async Task<int> Add(Car entity)
        //{
        //    var rs = _context.Add(entity).Result;
        //    return (rs);
        //}

        //public async Task<int> Update(Car entity)
        //{
        //    var rs = _context.Update(entity).Result;
        //    return (rs);
        //}

        public void Remove(Car entity)
        {
            _context.Remove(entity);
        }

        void ICar.Add(Car entity)
        {
            _context.Add(entity);
        }

        void ICar.Update(Car entity)
        {
            _context.Update(entity);
        }
    }
}
