﻿using Service.Interface;
using System;
using System.Collections.Generic;
using System.Text;
using Model.Entities;
using System.Threading.Tasks;
using Model;
using System.Linq;
using Service.Repository;

namespace Service.Implementation
{
    public class EmployeeService : IEmployee
    {
        protected readonly IGenericRepository<Employee> _context;
        
        public EmployeeService(IGenericRepository<Employee> context)
        {
            _context = context;
        }
        //async Task<int> Add(Employee entity)
        //{
        //    entity.Id = 0;
        //    var rs = _context.Add(entity).Result;
        //    return (rs);
        //}
        //async Task<int> Update(Employee entity)
        //{
        //    var rs = _context.Update(entity).Result;
        //    return (rs);
        //}

        //IEnumerable<Employee> GetAll()
        //{
        //    var lst = _context.GetAll();
        //    return lst;
        //}

        //Employee GetById(int id)
        //{
        //    var obj = _context.GetById(id);
        //    return obj;
        //}

        //void Remove(Employee entity)
        //{
        //    _context.Remove(entity);
        //}

        async Task<string> IEmployee.SayHello()
        {
            return "Hello Employee";
        }

        public IQueryable<Employee> GetAll_withforekey()
        {
            throw new NotImplementedException();
        }

        public IQueryable<Employee> Querry()
        {
            throw new NotImplementedException();
        }

        IEnumerable<Employee> IEmployee.GetAll()
        {
            var lst = _context.GetAll();
            return lst;
        }

        Employee IEmployee.GetById(int id)
        {
            var obj = _context.GetById(id);
            return obj;
        }

        async Task<int>  IEmployee.Add(Employee entity)
        {
            entity.Id = 0;
            //var rs = _context.Add(entity).Result;
            //return (rs);
            return (0);
        }

        async Task<int> IEmployee.Update(Employee entity)
        {
            //var rs = _context.Update(entity).Result;
            //return (rs);
            return (0);
        }

        void IEmployee.Remove(Employee entity)
        {
            _context.Remove(entity);
        }
    }
}
