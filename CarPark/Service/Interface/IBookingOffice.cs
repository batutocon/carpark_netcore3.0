﻿using Model.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Service.Repository;
using Model.ViewModels;
using System.Linq;

namespace Service.Interface
{
    public interface IBookingOffice
    {       
        public IEnumerable<BookingOffice> Filter(string key, string type, int pagesize, int page);
        public IEnumerable<BookingOffice> GetAll();
        BookingOffice GetById(int id);
        Task<int> Add(BookingOffice entity);
        Task<int> Update(BookingOffice entity);
        void Remove(BookingOffice entity);
    }
}
