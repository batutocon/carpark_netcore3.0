﻿using Model.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Service.Repository;
using Model.ViewModels;

namespace Service.Interface
{
    public interface ITicket
    {
        Ticket GetTicket(string otoid, int tripid);
        IEnumerable<Ticket> Filter(string key, int year, int month, int day, string type, int pagesize, int page);
        IEnumerable<Ticket> GetAll();
        Ticket GetById(int id);
        Task<int> Add(Ticket entity);
        Task<int> Update(Ticket entity);
        void Remove(Ticket entity);
    }
}
