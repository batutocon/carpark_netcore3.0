﻿using Model.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Service.Repository;
using Model.ViewModels;

namespace Service.Interface
{
    public interface ICar
    {
        Car GetByLicensePlate(string id);
        IEnumerable<Car> Filter(string key = "", string type = "STT", int pagesize = 3, int page = 1);
        IEnumerable<Car> GetAll();
        BookingOffice GetById(int id);
        void Add(Car entity);
        void Update(Car entity);
        void Remove(Car entity);
    }
}
