﻿using Model.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Service.Repository;

namespace Service.Interface
{
    public interface ITrip
    {
        public IEnumerable<Trip> Filter(string key,int year, int month, int day, string type, int pagesize, int page);
        public IEnumerable<Trip> GetAll();
        Trip GetById(int id);
        Task<int> Add(Trip entity);
        Task<int> Update(Trip entity);
        void Remove(Trip entity);
    }
}
