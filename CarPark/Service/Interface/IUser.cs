﻿using Model.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Service.Interface
{
    public interface IUser
    {
        Task<string> Authen(UserLogin rq);
        Task<bool> Register(UserRegister rq);
    }
}
