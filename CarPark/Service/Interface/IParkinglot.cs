﻿using Model.Entities;
using Model.ViewModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Service.Repository;

namespace Service.Interface
{
    public interface IParkinglot
    {
        public IEnumerable<Parkinglot> Filter(string key, string type, int pagesize, int page);
        public IEnumerable<Parkinglot> GetAll();
        Parkinglot GetById(int id);
        Task<int> Add(Parkinglot entity);
        Task<int> Update(Parkinglot entity);
        void Remove(Parkinglot entity);
    }
}
