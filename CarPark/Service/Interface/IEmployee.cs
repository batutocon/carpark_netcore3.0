﻿using Model.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Service.Repository;

namespace Service.Interface
{
    public interface IEmployee
    {
        Task<string> SayHello();
        IEnumerable<Employee> GetAll();
        Employee GetById(int id);
        Task<int> Add(Employee entity);
        Task<int> Update(Employee entity);
        void Remove(Employee entity);
    }
}
