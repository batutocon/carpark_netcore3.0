﻿using Service.Repository;
using System;
using System.Collections.Generic;
using System.Text;

namespace Service.Interface
{
    public interface IUnitOfWork
    {
        public GenericRepository<T> GetRepositoryInstance<T>() where T : class;
        void SaveChanges();
    }
}
