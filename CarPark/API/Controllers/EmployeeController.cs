﻿using Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Service.Interface;
using Model.Entities;
using Microsoft.AspNetCore.Authorization;

namespace API.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    public class EmployeeController : Controller
    {
        private readonly IEmployee _context;
        public EmployeeController(IEmployee context)
        {
            _context = context;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var lst = _context.GetAll();
            return Ok(lst);
        }

        [HttpGet ("{Id}")]
        public ActionResult Index([FromRoute] int Id)
        {
            var obj = _context.GetById(Id);
            return Ok(obj);
        }

        [HttpGet("sayhello")]
        public ActionResult Sayhello()
        {
            var rs = _context.SayHello();
            return Ok(rs.Result);
        }

        [HttpPost]
        public ActionResult Create([FromForm] Employee emp)
        {
            var rs = _context.Add(emp);
            if (rs.Result == 1)
            {
                return Ok("Create thanh cong");
            }
            else
            {
                return Ok("Create thatbai");
            }
        }
        [HttpPut("{Id}")]
        public ActionResult Update([FromRoute]int Id,[FromForm] Employee emp)
        {
            var obj = _context.GetById(Id);
            if (Id != obj.Id)
            {
                return Ok("Update khong thanh cong");
            }
            else
            {
                obj.Name = emp.Name;
                obj.Password = emp.Password;
                obj.Account = emp.Account;
                obj.Address = emp.Address;
                obj.Birthday = emp.Birthday;
                obj.Department = emp.Department;
                obj.Sex = emp.Sex;
                obj.Email = emp.Email;
                obj.Phone = emp.Phone;
                
                if (_context.Update(obj).Result == 1)
                {
                    return Ok("Update thanh cong");
                }
                else
                {
                    return Ok("Update thatbai");
                }
            }
        }
        [HttpDelete]
        public ActionResult Delete(int Id)
        {
            var obj = _context.GetById(Id);
            _context.Remove(obj);
            return Ok("Xoa thanh cong");
        }
    }
}
