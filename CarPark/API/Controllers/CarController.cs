﻿using Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Service.Interface;
using Model.Entities;
using Model.ViewModels;
using Microsoft.AspNetCore.Authorization;

namespace API.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    public class CarController : Controller
    {
        private readonly ICar _context;
        private readonly IUnitOfWork _uow;
        public CarController(ICar context, IUnitOfWork uow)
        {
            _context = context;
            _uow = uow;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var lst = _context.GetAll();
            return Ok(lst);
        }

        [HttpGet ("{Id}")]
        public ActionResult Index([FromRoute] string Id)
        {
            var obj = _context.GetByLicensePlate(Id);
            return Ok(obj);
        }

        [HttpPost]
        public ActionResult Create([FromForm] CarViewModelUpdateOrCreate request)
        {
            var obj = new Car();
            obj.ParkinglotId = request.ParkinglotId;
            obj.Price = request.Price;
            obj.LicensePlate = request.LicensePlate;
            obj.Type = request.Type;
            obj.Color = request.Color;
            obj.Company = request.Company;
            _uow.GetRepositoryInstance<Car>().Add(obj);
            try
            {
                _uow.SaveChanges();
                return Ok("Create thanh cong");
            }
            catch
            {
                return Ok("Create thatbai");
            }
        }

        [HttpGet("{type}/{pagesize}/{page}")]
        public ActionResult Filter(string key = "", [FromRoute] string type = "STT", [FromRoute] int pagesize = 3, [FromRoute] int page = 1)
        {
            var lst = _context.Filter(key, type, pagesize, page);
            return Ok(lst);
        }

        [HttpPut("{Id}")]
        public ActionResult Update([FromRoute]string Id,[FromForm] CarViewModelUpdateOrCreate request)
        {
            var obj = _context.GetByLicensePlate(Id);
            if (Id != obj.LicensePlate)
            {
                return Ok("Update khong thanh cong");
            }
            else
            {
                obj.ParkinglotId = request.ParkinglotId;
                obj.Price = request.Price;
                obj.Type = request.Type;
                obj.Color = request.Color;
                obj.Company = request.Company;

                _uow.GetRepositoryInstance<Car>().Update(obj);
                try
                {
                    _uow.SaveChanges();
                    return Ok("Update thanh cong");
                }
                catch
                {
                    return Ok("Update thatbai");
                }
            }
        }
        [HttpDelete]
        public ActionResult Delete(string Id)
        {
            var obj = _context.GetByLicensePlate(Id);
            _uow.GetRepositoryInstance<Car>().Remove(obj);
            try
            {
                _uow.SaveChanges();
                return Ok("Xoa thanh cong");
            }
            catch
            {
                return Ok("Xoa thatbai");
            }
        }
    }
}
