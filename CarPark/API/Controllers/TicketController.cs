﻿using Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Service.Interface;
using Model.Entities;
using System.Globalization;
using Model.ViewModels;
using Microsoft.AspNetCore.Authorization;

namespace API.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    public class TicketController : Controller
    {
        private readonly ITicket _context;
        private readonly IUnitOfWork _uow;
        public TicketController(ITicket context, IUnitOfWork uow)
        {
            _context = context;
            _uow = uow;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var lst = _context.GetAll();
            return Ok(lst);
        }

        [HttpGet("{otoid}/{tripid}")]
        public ActionResult Index([FromRoute] string otoid, [FromRoute] int tripid )
        {
            var obj = _context.GetTicket(otoid,tripid);
            return Ok(obj);
        }

        [HttpPost]
        public ActionResult Create([FromForm] TicketCreate request)
        {
            var obj = new Ticket();
            obj.BookAt = request.BookAt;
            obj.CustomerName = request.CustomerName;
            obj.LicensePlate = request.LicensePlate;
            obj.TripId = request.TripId;
            _uow.GetRepositoryInstance<Ticket>().Add(obj);
            try
            {
                _uow.SaveChanges();
                return Ok("Add thanh cong");
            }
            catch
            {
                return Ok("Add thatbai");
            }
        }
        [HttpPut("{otoid}/{tripid}")]
        public ActionResult Update([FromRoute] string otoid, [FromRoute] int tripid, [FromForm] TicketUpdate request)
        {
            var obj = _context.GetTicket(otoid, tripid);
            if (otoid != obj.LicensePlate || tripid != obj.TripId)
            {
                return Ok("Update khong thanh cong");
            }
            else
            {
                obj.BookAt = request.BookAt;
                obj.CustomerName = request.CustomerName;
                obj.LicensePlate = request.LicensePlate;
                obj.TripId = request.TripId;
                _uow.GetRepositoryInstance<Ticket>().Update(obj);
                try
                {
                    _uow.SaveChanges();
                    return Ok("Update thanh cong");
                }
                catch
                {
                    return Ok("Update thatbai");
                }
            }
        }
        [HttpDelete]
        public ActionResult Delete(string otoid, int tripid)
        {
            var obj = _context.GetTicket(otoid, tripid);
            _uow.GetRepositoryInstance<Ticket>().Remove(obj);
            try
            {
                _uow.SaveChanges();
                return Ok("Remove thanh cong");
            }
            catch
            {
                return Ok("Remove thatbai");
            }
        }
        [HttpGet("{type}/{pagesize}/{page}")]
        public ActionResult Filter(string key = "", int year = 0, int month = 0, int day = 0, [FromRoute] string type = "STT", [FromRoute] int pagesize = 3, [FromRoute] int page = 1)
        {
            var lst = _context.Filter(key, year, month, day, type, pagesize, page);
            return Ok(lst);
        }
    }
}
