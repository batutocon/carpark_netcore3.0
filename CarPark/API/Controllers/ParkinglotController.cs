﻿using Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Service.Interface;
using Model.Entities;
using Model.ViewModels;
using Microsoft.AspNetCore.Authorization;

namespace API.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    public class ParkinglotController : Controller
    {
        private readonly IParkinglot _context;
        private readonly IUnitOfWork _uow;
        public ParkinglotController(IParkinglot context, IUnitOfWork uow)
        {
            _context = context;
            _uow = uow;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var lst = _context.GetAll();
            return Ok(lst);
        }

        [HttpGet ("{Id}")]
        public ActionResult Index([FromRoute] int Id)
        {
            var obj = _context.GetById(Id);
            return Ok(obj);
        }

        [HttpGet("{type}/{pagesize}/{page}")]
        public ActionResult Filter(string key = "", [FromRoute] string type = "STT", [FromRoute] int pagesize = 3, [FromRoute] int page = 1)
        {
            var lst = _context.Filter(key, type, pagesize, page);
            return Ok(lst);
        }

        [HttpPost]
        public ActionResult Create([FromForm] ParkinglotCreate request)
        {
            var obj = new Parkinglot();
            obj.Name = request.Name;
            obj.Place = request.Place;
            obj.Price = request.Price;
            obj.Status = request.Status;
            obj.Area = request.Area;
            _uow.GetRepositoryInstance<Parkinglot>().Add(obj);
            try
            {
                _uow.SaveChanges();
                return Ok("Create thanh cong");
            }
            catch
            {
                return Ok("Create thatbai");
            }
        }
        [HttpPut("{Id}")]
        public ActionResult Update([FromRoute]int Id,[FromForm] ParkinglotUpdate request)
        {
            var obj = _context.GetById(Id);
            if (Id != obj.Id)
            {
                return Ok("Update khong thanh cong");
            }
            else
            {
                obj.Name = request.Name;
                obj.Place = request.Place;
                obj.Price = request.Price;
                obj.Status = request.Status;
                obj.Area = request.Area;
                _uow.GetRepositoryInstance<Parkinglot>().Update(obj);
                try
                {
                    _uow.SaveChanges();
                    return Ok("Update thanh cong");
                }
                catch
                {
                    return Ok("Update thatbai");
                }
            }
        }
        [HttpDelete]
        public ActionResult Delete(int Id)
        {
            var obj = _context.GetById(Id);
            _uow.GetRepositoryInstance<Parkinglot>().Remove(obj);
            try
            {
                _uow.SaveChanges();
                return Ok("Remove thanh cong");
            }
            catch
            {
                return Ok("Remove thatbai");
            }
        }
    }
}
