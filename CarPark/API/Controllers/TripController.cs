﻿using Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Service.Interface;
using Model.Entities;
using System.Globalization;
using Model.ViewModels;
using Microsoft.AspNetCore.Authorization;
using AutoMapper;

namespace API.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    public class TripController : Controller
    {
        readonly IUnitOfWork _unitOfWork;
        private readonly ITrip _context;
        private readonly IMapper _mapper;
        public TripController(IUnitOfWork unitOfWork,ITrip context, IMapper mapper)
        {
            _context = context;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var lst = _unitOfWork.GetRepositoryInstance<Trip>().GetAllRecords();
            return Ok(lst);
        }

        [HttpGet ("{Id}")]
        public ActionResult Index([FromRoute] int Id)
        {
            var obj = _context.GetById(Id);
            var model = _mapper.Map<TripCreate>(obj);
            return Ok(model);
        }

        [HttpPost]
        public ActionResult Create([FromForm] TripCreate request)
        {
            var obj = new Trip();
            obj.DepartureDate = request.DepartureDate;
            obj.DepartureTime = request.DepartureTime;
            obj.Destination = request.Destination;
            obj.Driver = request.Driver;
            obj.CarType = request.CarType;
            obj.MaxTicketNumerOnline = request.MaxTicketNumerOnline;
            obj.TicketNumber = request.TicketNumber;
            try
            {
                _unitOfWork.GetRepositoryInstance<Trip>().Add(obj);
                _unitOfWork.SaveChanges();
                return Ok("Create thanh cong");

            }
            catch
            {
                return Ok("Create thatbai");

            }
        }
        [HttpPut("{Id}")]
        public ActionResult Update([FromRoute]int Id,[FromForm] TripUpdate request)
        {
            var obj = _context.GetById(Id);
            if (Id != obj.Id)
            {
                return Ok("Update khong thanh cong");
            }
            else
            {
                obj.DepartureDate = request.DepartureDate;
                obj.DepartureTime = request.DepartureTime;
                obj.Destination = request.Destination;
                obj.CarType = request.CarType;
                obj.Driver = request.Driver;
                obj.MaxTicketNumerOnline = request.MaxTicketNumerOnline;
                obj.TicketNumber = request.TicketNumber;
                try
                {
                    _unitOfWork.GetRepositoryInstance<Trip>().Update(obj);
                    _unitOfWork.SaveChanges();
                    return Ok("Update thanh cong");

                }
                catch
                {
                    return Ok("Update thatbai");

                }
            }
        }
        [HttpDelete]
        public ActionResult Delete(int Id)
        {
            var obj = _context.GetById(Id);
            try
            {
                _unitOfWork.GetRepositoryInstance<Trip>().Remove(obj);
                _unitOfWork.SaveChanges();
                return Ok("Xoa thanh cong");

            }
            catch
            {
                return Ok("Update thatbai");

            }
        }

        [HttpGet("{type}/{pagesize}/{page}")]
        public ActionResult Filter(string key = "", int year=0, int month = 0, int day = 0, [FromRoute] string type = "STT", [FromRoute] int pagesize = 3, [FromRoute] int page = 1)
        {
            if (key==null)
            {
                key = "";
            }
            var lst = _context.Filter(key, year,month,day, type, pagesize, page).ToList();
            var model = _mapper.Map<IEnumerable<TripCreate>>(lst);
            return Ok(model);
        }
    }
}
