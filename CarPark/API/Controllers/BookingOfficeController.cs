﻿using Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Service.Interface;
using Model.Entities;
using Model.ViewModels;
using Model.Stored;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Data.SqlClient;
using OfficeOpenXml;
using System.IO;
using Microsoft.Extensions.Logging;

namespace API.Controllers
{
    [ApiController]
    //[Authorize]
    [Route("api/[controller]")]
    public class BookingOfficeController : Controller
    {
        private readonly IBookingOffice _context;
        private readonly IUnitOfWork _uow;
        private readonly ILogger<BookingOfficeController> _logger;
        public BookingOfficeController(IBookingOffice context, IUnitOfWork uow, ILogger<BookingOfficeController> logger)
        {
            _context = context;
            _uow = uow;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult Index()
        {
            var lst = _context.GetAll();
            return Ok(lst);
        }

        [HttpGet("{type}/{pagesize}/{page}")]
        public ActionResult Filter(string key = "", [FromRoute] string type = "STT", [FromRoute] int pagesize = 3, [FromRoute] int page = 1)
        {
            var lst = _context.Filter(key, type, pagesize, page);
            return Ok(lst);
        }

        [HttpGet("{Id}")]
        public ActionResult Index([FromRoute] int Id)
        {
            var obj = _context.GetById(Id);
            return Ok(obj);
        }

        [HttpPost]
        public ActionResult Create([FromForm] BookingOfficeCreate request)
        {
            var obj = new BookingOffice();
            obj.Name = request.Name;
            obj.Phong = request.Phong;
            obj.Place = request.Place;
            obj.Price = request.Price;
            obj.StartContractDeadLine = request.StartContractDeadLine;
            obj.EndContractDeadLine = request.EndContractDeadLine;
            obj.TripId = request.TripId;
            _uow.GetRepositoryInstance<BookingOffice>().Add(obj);
            try
            {
                _uow.SaveChanges();
                return Ok("Create thanh cong");
            }
            catch
            {
                return Ok("Create that bai");
            }
        }
        [HttpPut("{Id}")]
        public ActionResult Update([FromRoute] int Id, [FromForm] BookingOfficeUpdate request)
        {
            var obj = _context.GetById(Id);
            if (Id != obj.Id)
            {
                return Ok("Update khong thanh cong");
            }
            else
            {
                obj.Name = request.Name;
                obj.Phong = request.Phong;
                obj.Place = request.Place;
                obj.Price = request.Price;
                obj.StartContractDeadLine = request.StartContractDeadLine;
                obj.EndContractDeadLine = request.EndContractDeadLine;
                obj.TripId = request.TripId;
                _uow.GetRepositoryInstance<BookingOffice>().Update(obj);
                try
                {
                    _uow.SaveChanges();
                    return Ok("Update thanh cong");
                }
                catch
                {
                    return Ok("Update that bai");
                }
            }
        }
        [HttpDelete]
        public ActionResult Delete(int Id)
        {
            var obj = _context.GetById(Id);
            _uow.GetRepositoryInstance<BookingOffice>().Remove(obj);
            try
            {
                _uow.SaveChanges();
                return Ok("Remove thanh cong");
            }
            catch
            {
                return Ok("Remove that bai");
            }
        }
        [HttpGet("XuatExcel")]
        public async Task<IActionResult> XuatExcel(string key)
        {
            try
            {
                await Task.Yield();
                var keySearch = key == null ? "" : key.ToString().Trim();
                var txtSearch = new SqlParameter("@keySearch", System.Data.SqlDbType.NVarChar) { Value = keySearch };
                var data = _uow.GetRepositoryInstance<BookingOfficesDaCoTrip>().GetResultBySqlProcedure("BookingOfficesDaCoTrip @keySearch", txtSearch).ToList();
                var stream = new MemoryStream();
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                using (var package = new ExcelPackage(stream))
                {
                    var sheet = package.Workbook.Worksheets.Add("List Booking Office had Trip");
                    sheet.Cells[1, 1].Value = "STT";
                    sheet.Cells[1, 2].Value = "ID";
                    sheet.Cells[1, 3].Value = "End Contract Day";
                    sheet.Cells[1, 4].Value = "Start Contract Day";
                    sheet.Cells[1, 5].Value = "Name";
                    sheet.Cells[1, 6].Value = "Phone";
                    sheet.Cells[1, 7].Value = "Place";
                    sheet.Cells[1, 8].Value = "Price";
                    var row = 2;
                    foreach (var obj in data)
                    {
                        sheet.Cells[row, 1].Value = row;
                        sheet.Cells[row, 2].Value = obj.Id;
                        sheet.Cells[row, 3].Value = obj.EndContractDeadLine.ToString("dd/MM/yyyy");
                        sheet.Cells[row, 4].Value = obj.StartContractDeadLine.ToString("dd/MM/yyyy");
                        sheet.Cells[row, 5].Value = obj.Name;
                        sheet.Cells[row, 6].Value = obj.Phong;
                        sheet.Cells[row, 7].Value = obj.Place;
                        sheet.Cells[row, 8].Value = obj.Price;
                        row++;
                    }
                    package.Save();
                }
                stream.Position = 0;
                string filename = "List Booking Office had Trip.xlsx";
                return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", filename);
            }
            catch (Exception ex)
            {
                _logger.LogCritical("Xuat Excel loi luc {0} ex: {1}",DateTime.UtcNow,ex.ToString());
                return null;
            }

        }
    }
}
