﻿using Model.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Stored
{
    public class BookingOfficesDaCoTrip
    {
        public long STT { get; set; }
        public int Id { get; set; }
        public DateTime EndContractDeadLine { get; set; }
        public DateTime StartContractDeadLine { get; set; }
        public string Name { get; set; }
        public string Phong { get; set; }
        public string Place { get; set; }
        public double Price { get; set; }
        public int TripId { get; set; }
        public int TicketNumber { get; set; }
        public DateTime DepartureDate { get; set; }
        public DateTime DepartureTime { get; set; }
        public string Destination { get; set; }
        public string Driver { get; set; }
        public int MaxTicketNumerOnline { get; set; }
        public string CarType { get; set; }
    }
}
