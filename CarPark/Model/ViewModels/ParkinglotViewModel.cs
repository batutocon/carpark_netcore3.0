﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Model.ViewModels
{
    public class ParkinglotCreate
    {
        public int Area { get; set; }
        public string Name { get; set; }
        public string Place { get; set; }
        public double Price { get; set; }
        public int Status { get; set; }
    }
    public class ParkinglotUpdate
    {
        public int Id { get; set; }
        public int Area { get; set; }
        public string Name { get; set; }
        public string Place { get; set; }
        public double Price { get; set; }
        public int Status { get; set; }
    }
}
