﻿using System;
using System.Collections.Generic;
using System.Text;
using Model.Entities;

namespace Model.ViewModels
{
    public class BookingOfficeCreate
    {
        public DateTime EndContractDeadLine { get; set; }
        public DateTime StartContractDeadLine { get; set; }
        public string Name { get; set; }
        public string Phong { get; set; }
        public string Place { get; set; }
        public double Price { get; set; }
        public int TripId { get; set; }
    }
    public class BookingOfficeUpdate
    {
        public int Id { get; set; }
        public DateTime EndContractDeadLine { get; set; }
        public DateTime StartContractDeadLine { get; set; }
        public string Name { get; set; }
        public string Phong { get; set; }
        public string Place { get; set; }
        public double Price { get; set; }
        public int TripId { get; set; }
    }
}
