﻿using Model.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Model.ViewModels
{
    public class TicketCreate
    {
        public DateTime BookAt { get; set; }
        public string CustomerName { get; set; }
        public string LicensePlate { get; set; }
        public int TripId { get; set; }
    }
    public class TicketUpdate
    {
        public int Id { get; set; }
        public DateTime BookAt { get; set; }
        public string CustomerName { get; set; }
        public string LicensePlate { get; set; }
        public int TripId { get; set; }
    }
}
