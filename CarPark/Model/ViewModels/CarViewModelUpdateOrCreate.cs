﻿using Model.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Model.ViewModels
{
    public class CarViewModelUpdateOrCreate
    {
        public string LicensePlate { get; set; }
        public string Color { get; set; }
        public string Type { get; set; }
        public string Company { get; set; }
        public double Price { get; set; }
        public int ParkinglotId { get; set; }
    }
}
