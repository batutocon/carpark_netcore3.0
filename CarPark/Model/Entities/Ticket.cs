﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Entities
{
    public class Ticket
    {
        public int Id { get; set; }
        public DateTime BookAt { get; set; }
        public string CustomerName { get; set; }
        public string LicensePlate { get; set; }
        public int TripId { get; set; }
        public Car Car { get; set; }
        public Trip Trip { get; set; }
    }
}
