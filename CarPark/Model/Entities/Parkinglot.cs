﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Model.Entities
{
    public class Parkinglot
    {
        public int Id { get; set; }
        public int Area { get; set; }
        public string Name { get; set; }
        public string Place { get; set; }
        public double Price { get; set; }
        public int Status { get; set; }
    }
}
