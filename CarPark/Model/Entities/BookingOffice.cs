﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Entities
{
    public class BookingOffice
    {
        public int Id { get; set; }
        public DateTime EndContractDeadLine { get; set; }
        public DateTime StartContractDeadLine { get; set; }
        public string Name { get; set; }
        public string Phong { get; set; }
        public string Place { get; set; }
        public double Price { get; set; }
        public int TripId { get; set; }
        public Trip Trip { get; set; }
    }
}
