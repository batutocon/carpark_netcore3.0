﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Model.Entities
{
    public class Trip
    {
        public int Id { get; set; }
        public int TicketNumber { get; set; }
        public DateTime DepartureDate { get; set; }
        public DateTime DepartureTime { get; set; }
        public string Destination { get; set; }
        public string Driver { get; set; }
        public int MaxTicketNumerOnline { get; set; }
        public string CarType { get; set; }
    }
}
