﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Model.Entities
{
    public class Car
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string LicensePlate { get; set; }
        public string Color { get; set; }
        public string Type { get; set; }
        public string Company { get; set; }
        public double Price { get; set; }
        public int ParkinglotId { get; set; }
        public Parkinglot Parkinglot { get; set; }
    }
}
